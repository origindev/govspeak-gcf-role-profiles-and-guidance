##Cross Pillar: Category Management

###Commercial Professional Level - Commercial Specialist (Senior Civil Servant 1 equivalent)

###Role summary

In this role you will lead and own delivery across the commercial lifecycle. You will contribute to delivering commercial excellence for the department, providing a customer-focused service, understanding the business need and stakeholder drivers, with the aim of becoming a trusted commercial advisor. You will develop close relationships with the department’s key and strategic suppliers, driving ‘value for money’ and risk management excellence across the category.  

You will be expected to manage and/or oversee the overall commercial lifecycle. Activities could vary from developing the category strategy to leading complex procurements to being accountable for some of the department’s most complex and high profile contracts. You will be expected to manage challenging senior stakeholders such as ministers and director general level internal stakeholders, delivering difficult messages whilst maintaining strong relationships and ensuring end-to-end commercial activity complies with the department’s legislative framework. 

You will lead a team, building their confidence and commercial capability as an influential leader to shape and define a deal in a complex and uncertain context, delegating risks and issues to individuals in line with your experience and potential. The role will also require you to use your commercial expertise to take measured risks in order to deliver better approaches and services.

###Key Responsibilities

Category Management roles are varied, and individual remit is subject to both size of category, and/or level of complexity, value and risk within a category. 

Typically, key responsibilities for a commercial professional in this role may include (but are not limited to):

- Leading a team of commercial professionals: 
  - Educating, advising, challenging and guiding on commercial priorities
  - Encouraging ideas, improvements and measured risk-taking to deliver better approaches and services
  - Performance Management of the Category Team 

- Strategic delivery and management of one or more commercial categories, based on size, complexity and risk:
  - Developing and delivering commercial strategies requiring supplier innovation, senior stakeholder engagement, and/or market building/ shaping
  - Accountable for category strategies for relevant expenditure within the pillar
  - Ensuring category management processes are in place that enable the assessment of the market capabilities to meet the needs of users and the business
  - Driving innovation in category to demonstrate continuous improvement
  - Providing advice on major investment appraisal and decisions

- Understanding relevant sourcing options, delivering positive outcomes through the procurement process: 
  - Accountable for sourcing activity within the category/categories, including strategic sourcing processes in support of the department’s strategic aims
  - Assessing and recommending optimum supply strategy from a range of make vs buy commercial options (insourcing, outsourcing, investment, PPP, mutual, third sector, etc)
  - Leading Commercial input into business cases, to demonstrate the value, benefits and risks of sourcing activity
  - Leading complex and high value negotiations, finalising major deals, escalating only where necessary to secure progress or improve outcomes
  - Leading on overseeing the strategy for transition from procurement/ sourcing to contract manager 

- Ensuring effective Commercial Contract and Supplier Management within the category:
  - Managing costs and driving the best value out of contracts
  - Championing continuous improvement, aligning strategies with wider government commercial policies, taking into account internal and external forces and best practice, e.g. National Audit Office, Professional Associations
  - Using a variety of contracting approaches to minimise risk, motivate performance, and drive innovation
  - At a management level, across contracts, work with key suppliers and customers, driving compliance with contractual commitments
  - Critically assessing supply chains, seeking to protect the department’s risk exposure 
  - Developing and enacting exit strategies to be used across contracts

- Building and maintaining key relationships across the department and with important external stakeholders:
  - Owning relationships and act as the business partner for area of responsibility, building detailed knowledge of business needs in order to deliver value through sourcing and procurement activity 

- Enabling and developing the department: 
  - Working with peers to drive an improvement in commercial capability, including through designing and improving learning and development programmes, aligning with Government Commercial Function initiatives, sharing best practice and templates
  - Promoting commercial awareness across the department
  - Promoting knowledge and resource sharing with peers and across functions

###Essential Criteria

<strong>Category Management: </strong>

- Has excellent understanding of the principles of end-to-end commercial lifecycle management, procurement processes and ability to demonstrate effective management of the market for the category 
- Evidence of taking a strategic lead on securing ‘value for money’  in a complex commercial environment
- Evidence of influencing key senior stakeholders and suppliers towards commercial solutions in a complex commercial environment
- Evidence of leading and delivering transformational change at a strategic level

 <strong>Commercial Focus:</strong> 

- Has a strong practical understanding of market shaping principles and contract law and has experience in working in a range of categories/sectors with a range of suppliers 
- Demonstrates the ability to develop the definition of standards of success in terms of the return on investment and ‘value for money’ for each deal and can lead the debate to make the case with ministers
- Demonstrates the ability to manage the trade-offs and contradictions within complex deals and impact on other areas

<strong>Risk and Assurance Management:</strong> 

- Demonstrates the ability to take measured risks in order to deliver better approaches and services
- Demonstrates the ability to challenge the analysis of risks and margins of error to improve assurance on decisions 

<strong>Commercial Ethics:</strong> 

- Demonstrates the ability to contribute to the development of standards and/or policy to eradicate corruption, fraud and unethical behaviour

<strong>Team Management:</strong> 

- Demonstrates the ability to coach and mentor individuals as a trusted leader and displays strong motivational capabilities to create a department culture focused on performance and priorities
- Demonstrates the ability to manage a team in shaping and defining a deal in a complex and uncertain context, delegating risks and issues to individuals in line with their experience and potential

<strong>Build Relationships: </strong>

- Has strong stakeholder engagement skills in order to effectively communicate a deal over time from conception to delivery in line with the department’s commercial requirements
- Demonstrates the ability to understand the motivations and behaviours of stakeholders, suppliers and networks

###Civil Service Behaviours

Ability to show examples across all of the following behaviours for level 5 of the <a href="https://assets.publishing.service.gov.uk/government/uploads/system/uploads/attachment_data/file/717275/CS_Behaviours_2018.pdf">Success Profiles Framework</a>:

- <strong>Managing a Quality Service:</strong> Clarify and articulate the diverse requirements of customers and delivery partners. Use customer insight to drive high quality and efficient service delivery which is a good investment of taxpayers money. Work collaboratively with customers and delivery partners to manage, monitor and deliver agreed outcomes. Identify areas for improvement and make appropriate changes to reach professional excellence. Break down complex aims into clear, practical and manageable plans. Identify the resource requirements to support implementation. Ensure risks are monitored and managed to prevent issues with service delivery wherever possible

- <strong>Working Together:</strong> Proactively create, maintain and promote a strong network of contacts across the organisation and externally. Embed an inclusive culture of creating positive and supportive teams who consider the diverse needs and feelings of other colleagues. Ensure consideration and support for the wellbeing of all individuals across the organisation. Set out clear expectations that bullying, harassment, and discrimination are unacceptable. Encourage and establish mechanisms to share knowledge and resources across boundaries to support the business. Encourage teams to engage with a variety of delivery partners and stakeholders, listen to and act on their feedback

- <strong>Changing and Improving:</strong> Challenge the way things have always been done and suggest improvements, learning from experience. Seek, encourage and recognise initiative and imaginative ideas from a wide range of people. Promote an environment where all colleagues feel safe to challenge. Encourage measured risk taking and innovation to deliver better approaches and services. Implement changes that transform flexibility, responsiveness and quality of service. Ensure changes add value to the business and express clearly how and why changes are necessary. Lead the transformation towards using digital technologies ensuring full consideration of accessibility needs and the diverse range of end users. Manage change effectively and respond promptly to critical events. Constructively challenge changes which are unhelpful

- <strong>Leadership:</strong> Remain visible and approachable to all colleagues and stakeholders. Actively promote the reputation of the organisation with pride, both internally and externally. Display passion and enthusiasm for the work, helping to inspire colleagues and stakeholders to fully engage with the aims and long term vision. Embed a culture of inclusion and equal opportunity for all, where the diversity of individuals’ backgrounds and experiences are valued and respected. Work to influence the strategy, direction and culture to increase effectiveness

###Department Context

To enhance the generic commercial role profile, additional information can be added by a department to outline the specific nature of the role. This may include: 

- An additional short paragraph in the role summary 
- Additional key responsibilities based on the department context 
- Bespoke essential skills/experience required for the role