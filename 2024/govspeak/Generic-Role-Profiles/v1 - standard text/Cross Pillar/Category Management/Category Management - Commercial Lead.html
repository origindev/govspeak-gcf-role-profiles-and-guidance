##Cross Pillar: Category Management

###Commercial Professional Level - Commercial Lead (Grade 7 equivalent)

###Role summary

In this role you will contribute to delivering commercial excellence for the department, providing a customer-focused service, understanding the business need and stakeholder drivers, with the aim of becoming a trusted commercial advisor. You will also develop relationships with the department’s key and strategic suppliers, driving value for money and risk management excellence across the category.  

You will lead a team, building their confidence to drive performance and manage setbacks. The role will also require you to use your commercial expertise to identify opportunities for improvement and implement these changes at a local level by working with affected parties to identify and overcome challenges.

You will lead and own elements of the overall commercial lifecycle, including strategy and policy development, understanding needs and sourcing options, contract and supplier management and the procurement process itself. This will be achieved through the development and execution of category management strategies, leading supplier negotiations, where needed, and delivering timely and effective support, guidance and expertise to the business.  

###Key Responsibilities

Category Management roles are varied, and individual remit is subject to both size of category, and/or level of complexity, value and risk within a category. 

Typically, key responsibilities for a commercial professional in this role may include (but are not limited to): 

- Strategic delivery and management within a category:
  - Developing commercial approaches that deliver requirements that are in line with business needs, category strategies and market trends
  - Contributing to the development of category strategies and delivery of sourcing requirements within commercial, as part of the team supporting business as usual and project activities
  - Understanding demand by forecasting and planning requirements with internal stakeholders and suppliers
  - Developing and maintaining category plans for key spend areas

- Understanding relevant sourcing options and analysis, delivering positive outcomes through the procurement process: 
  - Performing end-to-end strategic sourcing activities for the department and serving as a subject matter expert
  - Working with key stakeholders to develop a clear and agreed view of business requirements, supporting business units in articulating their commercial requirements
  - Advising and providing professional guidance throughout the sourcing process, considering and evaluating a range of sourcing models
  - Supporting the development of options for a business case, including articulating associated benefits and producing supporting arguments for the preferred option 
  - Identifying opportunities to develop collaborative partnerships with suppliers
  - Providing a procurement service to the department which delivers demonstrable value for money and compliant goods and services contracts as part of a team of procurement professionals on larger procurement activities
  - Leading and guiding others with negotiation relating to commercial activity, using knowledge and experience with negotiation techniques
  - Understanding and being accountable for sourcing compliance and frameworks 

- Ensuring effective Commercial Contract and Supplier Management within the category:
  - Managing contract variations, providing practical advice and strategic direction
  - Evaluating whether a contract is achieving a successful return on investment, in the light of value for money considerations
  - Overseeing the realisation of benefits achieved as a result of the performance management regime or change control, and challenging non-delivery of benefits and escalates when required
  - Evaluating the performance of suppliers against the specification and reporting regularly on progress and/or challenges
  - Managing risk and reward mechanisms in contracts and key performance indicators
  - Monitoring progress against the business objectives and works with suppliers and customers to develop improvements
  - Actively managing strategic relationships to deliver mutual benefit for those involved
  - At a delivery level, holding both suppliers and customers accountable for delivering their contractual commitments. Resolving disputes effectively and efficiently
  - Performing contract exit activity, from pre-emptive action to transactional, process led tasks
  - Managing the disposal process of assets that are no longer needed once the current contract expires
  - Advising on and mitigating risks to the continuity of supply, and owning business continuity approach for relevant contract(s)

- Building and maintaining key relationships across the department and with important external stakeholders:
  - Developing key stakeholder relationships, enabling early engagement and challenging business need and demand. Seeking early active involvement of internal stakeholders in the sourcing process
  - Providing commercial advice to stakeholders on all aspects of commercial protocol
  - Engaging, where required, with the relevant senior business lead, ensuring that business requirements are supported by category strategies

- Enabling and developing the department: 
  - Feeding into catalogue and content management for the department, setting the direction for the category 

- Performance and day-to-day management of category team, where applicable 
- Working within a secure environment on sensitive projects as required

###Essential Criteria

<strong>Category Management:</strong> 

- Has excellent understanding of the principles of end-to-end commercial lifecycle management 
- Evidence of taking a strategic lead on securing value for money in a complex commercial environment
- Evidence of influencing key senior stakeholders and suppliers towards commercial solutions in a complex commercial environment

<strong>Commercial Focus: </strong>

- Has strong commercial knowledge, understands the need for trade-offs in an area of responsibility, can apply the commercial principles within the boundaries of contract law
- Demonstrates the ability to lead on securing value for money through achieving good return on investment on commercial projects

<strong>Risk and Assurance Management:</strong> 

- Demonstrates the ability to analyse risks and understand margins of error in developing recommendations and making effective decisions

<strong>Commercial Ethics:</strong> 

- Demonstrates the ability to communicate standards to eliminate corruption, fraud and unethical behaviour in supply chains, taking appropriate actions in the event of any alleged breach of standards

<strong>Team Focus:</strong> 

- Demonstrates the ability to manage and build confidence in a team in order to deal with setbacks, resolve internal issues and can implement change at a local level by working with affected parties to identify and overcome challenges

<strong>Build Relationships:</strong> 

- Has strong stakeholder engagement skills and can present technical advice successfully in order to influence internal and external decision making. 
- Demonstrates the ability to articulate commercial motivations and expected behaviours of stakeholders, suppliers and networks

###Civil Service Behaviours

Ability to show examples across all of the following behaviours for level 4 of the <a href="https://assets.publishing.service.gov.uk/government/uploads/system/uploads/attachment_data/file/717275/CS_Behaviours_2018.pdf">Success Profiles Framework</a>:

- <strong>Managing a Quality Service:</strong> Demonstrate positive customer service by understanding the complexity and diversity of customer needs and expectations. Deliver a high quality, efficient and cost effective service by considering a broad range of methods for delivery. Ensure full consideration of new technologies, accessibility and costings. Make clear, practical and manageable plans for service delivery. Ensure adherence to legal, regulatory and security requirements in service delivery. Proactively manage risks and identify solutions. Establish how the business area compares to industry best practice. Create regular opportunities for colleagues, stakeholders, delivery partners and customers to help improve the quality of service

- <strong>Working Together:</strong> Actively build and maintain a network of colleagues and contacts to achieve progress on shared objectives. Challenge assumptions while being willing to compromise if beneficial to progress. Build strong interpersonal relationships and show genuine care for colleagues. Ensure consideration and support for the wellbeing of yourself and individuals throughout the team. Understand the varying needs of the team to ensure they are supported and their experiences are utilised. Create an inclusive working environment where all opinions and challenges are taken into account and bullying, harassment and discrimination are unacceptable. Remain available and approachable to all colleagues and be receptive to new ideas

- <strong>Developing Self and Others:</strong> Prioritise and role-model continuous self-learning and development. Identify areas individuals and teams need to develop in order to achieve future objectives. Support colleagues to take responsibility for their own learning and development. Ensure that development opportunities are available for all individuals regardless of their background or desire to achieve promotion. Ensure individuals take full advantage of learning and development opportunities available to them, including workplace based learning. Encourage discussions within and between teams to learn from each other’s experiences and change organisational plans and processes accordingly

- <strong>Leadership:</strong> Promote diversity, inclusion and equality of opportunity, respecting difference and external experience. Welcome and respond to views and challenges from others, despite any conflicting pressures to ignore or give in to them. Stand by, promote or defend own and team’s actions and decisions where needed. Seek out shared interests beyond own area of responsibility, understanding the extent of the impact actions have on the organisation. Inspire and motivate teams to be fully engaged in their work and dedicated to their role 

###Department Context

To enhance the generic commercial role profile, additional information can be added by a department to outline the specific nature of the role. This may include:

- An additional short paragraph in the role summary 
- Additional key responsibilities based on the department context 
- Bespoke essential skills/experience required for the role