##Cross Pillar: Category Management

###Commercial Professional Level - Commercial Practitioner (Higher Executive Officer equivalent)

###Role summary

In this role you will be expected to contribute to delivering commercial excellence for the department, providing a customer-focused service, understanding the business need and stakeholder drivers, with the aim of becoming a trusted commercial advisor. You will also develop relationships with the department’s key and strategic suppliers, driving value for money and risk management excellence across the category. 

You will be expected to support across the overall commercial life cycle. You will contribute to strategy and policy development, analyse data and use market insight to support the sourcing options, and work across the procurement process. 

You will work as part of a larger team, but will be expected to work independently by prioritising and managing your own workload to deliver to deadlines and to respond to changing operational demands.

###Key Responsibilities

Category Management roles are varied, and individual remit is subject to both size of category, and/or level of complexity, value and risk within a category. 

Typically, key responsibilities for a commercial professional in this role may include (but are not limited to): 

- Strategic delivery and management within a category:
  - Supporting development of specifications to deliver requirements that are in line with business needs, and existing category strategy 
  - Engaging, where required, with the relevant lead ensuring that business requirements are in line with existing category strategies
  - Contributing to the development of category strategies
  - Carrying out analysis, research and requirements gathering to feed into demand forecasting and category strategies
  - Tracking progress against category plans and working to deliver key tasks 

- Understanding relevant sourcing options and analysis, delivering positive outcomes through the procurement process: 
  - Working independently across end-to-end strategic sourcing activities for the department
  - In some cases, leading on low value/low complexity procurements, with support from senior commercial colleagues
  - Engaging with key stakeholders across the department to support the team develop a clear and agreed view of business requirements
  - Advising and providing professional guidance throughout the sourcing process, supporting the evaluation of a range of sourcing models
  - Gathering inputs and at times leading development of commercial input into business cases, evaluating the benefits, value and risks of projects
  - Providing a procurement service to the department which delivers demonstrable value for money and compliant goods and services contracts as part of a team of procurement professionals on larger procurement activities
  - Understanding, and being able to advise on, sourcing compliance and frameworks 

- Ensuring effective commercial contract and supplier management within the category:
  - Day-to-day management of one or more low to medium value/risk/ complexity contracts within the category, with support from senior commercial colleagues
  - Managing one or more contracts/suppliers, ensuring effective stakeholder management, governance, performance management frameworks, escalation, risk management, issues resolution, financial management, change control and compliance
  - Contributing to the management and administration of the whole supply chain portfolio, including the implementation of supply chain management and supplier relationship strategies
  - Supporting the administration of change control notices, data/spend analysis, management information and reporting
  - Tracking and owning risks relating to specific contracts, taking steps to mitigate commercial risks
  - Identifying perceived areas of risk, and supporting analysis of suppliers for risk and assurance purposes

- Building and maintaining key relationships across the department and with important external stakeholders:
  - Developing effective working relationships with supplier organisations to establish appropriate governance and relationship arrangements, ensuring the effective management of clarifications and change requests
  - Engaging and working closely with the relevant senior business lead, ensuring that business requirements are supported by category strategies
  - Identifying key suppliers and articulating management and engagement level required to assure the supply chain
  - Assisting with senior internal stakeholder management across the business, helping them be informed about business elements that aim to move forward critical decision making at the top level
  - Assisting with senior external stakeholder management, holding working level relationships with individuals at external stakeholder groups

- Enabling and developing the department: 
  - Contributing to the maintenance and updating of systems throughout the procurement lifecycle
  - Feeding into catalogue and content management for the department, owning key aspects and working with stakeholders to promote the use of catalogues   

- Working within a secure environment on sensitive projects as required

###Essential Criteria

<strong>Category Management:</strong> 

- Has some experience within a procurement, commercial or buying role 
- Has a good understanding of the principles of end-to-end commercial lifecycle management 

<strong>Commercial Focus: </strong>

- Has an understanding of elementary economics factors that may determine a market shape, dynamics and models, the changes in the business needs of an organisation and is familiar with key commercial tools

<strong>Risk and Assurance Management:</strong> 

- Demonstrates the ability to identify and communicate cyber, personnel and physical risks and gain agreement on how they should be managed/mitigated

<strong>Commercial Ethics:</strong> 

- Demonstrates the ability to apply standards to eliminate corruption, fraud and unethical behaviour in supply chains, taking appropriate actions in the event of any alleged breach of standards

<strong>Team Focus:</strong>

- Demonstrates the ability to work as part of a team to support their performance, manage setbacks and resolve issues internally

<strong>Build Relationships:</strong> 

- Demonstrates the ability to communicate with stakeholders at all levels using different communication styles to articulate the benefits that have been achieved and to establish governance and relationship arrangements

###Civil Service Behaviours

Ability to show examples across the following behaviours for level 3 of the <a href="https://assets.publishing.service.gov.uk/government/uploads/system/uploads/attachment_data/file/717275/CS_Behaviours_2018.pdf">Success Profiles Framework</a>:

- <strong>Seeing the Big Picture:</strong> Understand the strategic drivers for your area of work. Align activities to contribute to wider organisational priorities. Remain alert to emerging issues and trends which might impact your work area. Seek out and share experiences to develop knowledge of the team’s business area. Understand how the strategies and activities of the team create value and meet the diverse needs of all stakeholders

- <strong>Changing and Improving:</strong> Work with others to identify areas for improvement and simplify processes to use fewer resources. Use technology where possible to increase efficiency. Encourage ideas for change from a wide range of sources. Clearly explain the reasons for change to colleagues and how to implement them, supporting individuals with different needs to adapt to change. Encourage an environment where colleagues know that they can challenge decisions and issues safely. Take managed risks by fully considering the varied impacts changes could have on the diverse range of end users

- <strong>Making Effective Decisions:</strong> Understand own level of responsibility and empower others to make decisions where appropriate. Analyse and use a range of relevant, credible information from internal and external sources to support decisions. Invite challenge and where appropriate involve others in decision making. Display confidence when making difficult decisions, even if they prove to be unpopular. Consult with others to ensure the potential impacts on end users have been considered. Present strong recommendations in a timely manner outlining the consideration of other options, costs, benefits and risks

- <strong>Leadership:</strong> Ensure colleagues and stakeholders have a clear understanding of objectives, activities and time-frames. Take into account different individual needs, views, and ideas, championing inclusion and equality of opportunity for all. Consider the impacts of own and team’s activities on stakeholders and end users. Role-model commitment and satisfaction with role. Recognise and praise the achievements of others to drive positivity within the team. Effectively manage conflict, misconduct and non-inclusive behaviour, raising with senior managers where appropriate

- <strong>Communicating and Influencing:</strong> Communicate in a straightforward, honest and engaging manner, choosing appropriate styles to maximise understanding and impact. Encourage the use of different communication methods, including digital resources and highlight the benefits, including ensuring cost effectiveness. Ensure communication has a clear purpose and takes into account people’s individual needs. Share information as appropriate and check understanding. Show positivity and enthusiasm towards work, encouraging others to do the same. Ensure that important messages are communicated with colleagues and stakeholders respectfully, taking into consideration the diversity of interests

- <strong>Working Together:</strong> Encourage joined up team work within own team and across other groups. Establish professional relationships with a range of stakeholders. Collaborate with these to share information, resources and support. Invest time to develop a common focus and genuine positive team spirit where colleagues feel valued and respect one another. Put in place support for the wellbeing of individuals within the team, including consideration of your own needs. Make it clear to all team members that bullying, harassment and discrimination are unacceptable. Actively seek and consider input of people from diverse backgrounds and perspectives

- <strong>Developing Self and Others:</strong> Identify capability gaps for self and team. Ensure development objectives are set and achieved to address any gaps and enable delivery of current and future work. Take time to coach, mentor and develop other colleagues to support succession planning. Promote inclusiveness by respecting different personal needs in the team and use these to develop others. Reflect on own work, continuously seek and act on feedback to improve own and team’s performance

- <strong>Managing a Quality Service:</strong> Develop, implement, maintain and review systems and services to ensure delivery of professional excellence. Work with stakeholders to set priorities, objectives and timescales. Successfully deliver high quality outcomes that meet the customers’ needs and gives ‘value for money’. Identify risks and resolve issues efficiently. Involve a diverse range of colleagues, stakeholders and delivery partners in developing suggestions for improvements. Establish ways to find and respond to feedback from customers about the services provided

- <strong>Delivering at Pace:</strong> Show a positive approach to keeping the whole team’s efforts focused on the top priorities. Promote a culture of following the appropriate procedures to ensure results are achieved on time whilst still enabling innovation. Ensure the most appropriate resources are available for colleagues to use to do their job effectively. Regularly monitor your own and team’s work against milestones ensuring individual needs are considered when setting tasks. Act promptly to reassess workloads and priorities when there are conflicting demands to maintain performance. Allow individuals the space and authority to meet objectives, providing additional support where necessary, whilst keeping overall responsibility

###Department Context

To enhance the generic commercial role profile, additional information can be added by a department to outline the specific nature of the role. This may include:

- An additional short paragraph in the role summary 
- Additional key responsibilities based on the department context 
- Bespoke essential skills/experience required for the role