##Cross Pillar: Category Management

###Commercial Professional Level - Associate Commercial Practitioner (Executive Officer equivalent)

###Role summary

In this role you will support across all aspects of the commercial lifecycle within the department. You will be expected to provide support to sourcing, procurement and contract management teams and staff. You will be required to analyse data and use market insight to help determine viable sourcing options and support the procurement process.

You will work as part of a larger team, but will be expected to be able to prioritise and manage your own workload to deliver to deadlines and know when to seek support in order to balance both operationally urgent and important tasks.

###Key Responsibilities

Category Management roles are varied, and individual remit is subject to both size of category, and/or level of complexity, value and risk within a category. 

Typically, key responsibilities for a commercial professional in this role may include (but are not limited to): 

- Providing administrative support to commercial colleagues on ad-hoc basis as part of the team supporting business as usual and project activities

- Strategic delivery and management within a category:

  - Assisting with development of category strategies for all relevant expenditure
  - Ensuring compliance with all relevant contracts
  - Engaging, where required, with the relevant lead ensuring that business requirements are in line with existing category strategies
  - Carrying out analysis, research and requirements gathering to feed into demand forecasting and category strategies

- Understanding relevant sourcing options and analysis, delivering positive outcomes through the procurement process: 
  - Supporting the sourcing process for a wide range of procurements, such as market engagement, supporting negotiations and assessing procurement routes
  - Maintaining routine catalogues within commercial systems to ensure they are up to date, liaising with colleagues as necessary
  - Working independently across end-to-end strategic sourcing activities for the department
  - Engaging with key stakeholders across the department to support the team develop a clear and agreed view of business requirements
  - Collating information for commercial input into business cases, demonstrating a project's benefits, ‘value for money’ and risk

- Ensuring effective Commercial Contract and Supplier Management within the category:

  - Undertaking contract management actions such as contract administration, monitoring supplier performance and managing supplier relationships
  - Providing commercial support and guidance to a range of internal and external stakeholders 
  - Supporting management of one or more contracts/suppliers, ensuring effective stakeholder management, governance, performance management frameworks, escalation, risk management, issues resolution, financial management, change control and compliance
  - Supporting the administration of change control notices, data/spend analysis, management information and reporting
  - Tracking and owning risks relating to specific contracts, taking steps to mitigate commercial risks
  - Identifying perceived areas of risk, and supporting analysis of suppliers for risk and assurance purposes

- Building and maintaining key relationships across the department and with important external stakeholders:

  - Assisting with senior internal stakeholder management across the business, helping them be informed about business elements that aim to move forward critical decision making at the top level
  - Assisting with senior external stakeholder management, holding working level relationships with individuals at external stakeholder groups

- Enabling and developing the department: 

  - Contributing to the maintenance and updating of systems throughout the procurement lifecycle
  - Feeding into catalogue and content management for the department, owning key aspects and working with stakeholders to promote the use of catalogues   

- Working within a secure environment on sensitive projects as required

###Essential Criteria

<strong>Category Management: </strong>

- Has basic understanding of the principles of end-to-end commercial lifecycle management 
- Has basic understanding of sourcing and procurement methodologies

<strong>Commercial Focus:</strong> 

- Has a good understanding of the elementary economics factors that may determine a market shape, dynamics and models and can apply tools and techniques associated with gathering commercial data

<strong>Risk and Assurance Management:</strong> 

- Demonstrates the ability to identify possible risks, understand their impact, use appropriate mitigation techniques and escalate them where appropriate

<strong>Commercial Ethics:</strong> 

- Has an understanding of the standards to eliminate corruption, fraud and unethical behaviour in supply chains and can take action in the event of any alleged breach of standards

<strong>Team Focus:</strong> 

- Demonstrates the ability to work as part of a team to support their resilience and performance

<strong>Build Relationships:</strong> 

- Has good stakeholder management skills and has the ability to use different communication styles with different audiences

###Civil Service Behaviours

Ability to show examples across the following behaviours for level 2 of the <a href="https://assets.publishing.service.gov.uk/government/uploads/system/uploads/attachment_data/file/717275/CS_Behaviours_2018.pdf">Success Profiles Framework</a>:

- <strong>Seeing the Big Picture:</strong> Understand how your work and the work of your team supports wider objectives and meets the diverse needs of stakeholders. Keep up to date with the issues that affect your work area. Take a keen interest in expanding knowledge in areas related to your work. Focus on overall goals and not just specific tasks to meet priorities

- <strong>Changing and Improving:</strong> Regularly review own and team’s work and take the initiative to suggest ideas to make improvements. Give feedback on changes in a constructive manner. Take a positive, open approach to the possibility of change and encourage others to do the same. Help others to understand changes and the reasons they are being put in place. Identify and act on the effects changes are having on your role and that of the team. Look for ways to use technology to achieve efficient and effective results. Consider accessibility needs of the diverse range of end users

- <strong>Making Effective Decisions:</strong> Take responsibility for making effective and fair decisions, in a timely manner. Analyse and research further information to support decisions. Talk to relevant people to get advice and information when unsure how to proceed. Explain how decisions have been reached in a clear and concise way, both verbally and in writing. Demonstrate the consideration of all options, costs, risks and wider implications, including the diverse needs of end users and any accessibility requirements

- <strong>Leadership:</strong> Show pride and passion for your work and positive, inclusive engagement with your team. Understand your areas of responsibility and display awareness of the wider impact of your actions. Proactively role model and promote an inclusive workplace, promptly dealing with inappropriate language and behaviours when they arise, including any instances of discrimination or misconduct. Give praise and credit to colleagues and stakeholders where appropriate

- <strong>Communicating and Influencing:</strong> Communicate clearly and concisely both orally and in writing. Take time to consider the best communication channel to use for the audience, including making the best of digital resources and considering ‘value for money’. Interact with others in an enthusiastic way. Express ideas clearly and with respect for others. Listen to and value different ideas, views and ways of working. Respond constructively and objectively to comments and questions. Handle challenging conversations with confidence and sensitivity

- <strong>Working Together:</strong> Develop a range of contacts outside own team and identify opportunities to share knowledge, information and learning. Show genuine interest when listening to others. Contribute to an inclusive working environment where all opinions and challenges are listened to and all individual needs are taken into account. Ensure it is clear that bullying, harassment and discrimination are unacceptable. Offer support and help to colleagues when in need, including consideration of your own and their wellbeing. Change ways of working to aid cooperation within and between teams in order to achieve results

- <strong>Developing Self and Others:</strong> Identify gaps in own and team’s skills and knowledge. Set and consistently meet development objectives. Seek learning opportunities. Support the development plans of all colleagues, recognising how diversity of experience/background can help to build an inclusive team culture. Consider the contributions of all team members and delegate work to aid the learning and development of all. Encourage and listen to developmental feedback from colleagues

- <strong>Managing a Quality Service: </strong>Work with customers to understand their needs and expectations. Create clear plans and set priorities which meet the needs of both the customer and the business. Clearly explain to customers what can be done. Keep colleagues and stakeholders fully informed of plans, possibilities and progress. Identify common problems that affect service, report them and find possible solutions. Deliver good customer service which balances quality and cost effectiveness

- <strong>Delivering at Pace:</strong> Regularly review the success of activities in the team to identify barriers to progress or challenging objectives. Identify who and what is required to ensure success, set clear goals and areas of responsibility and continually assess workloads considering individual needs. Follow relevant policies, procedures and legislation to complete your work. Ensure colleagues have the correct tools and resources available to them to do their jobs. Have a positive and focused attitude to achieving outcomes, despite any setbacks. Regularly check performance against objectives, making suggestions for improvement or taking corrective action where necessary. Ensure that colleagues are supported where tasks are challenging

###Department Context

To enhance the generic commercial role profile, additional information can be added by a department to outline the specific nature of the role. This may include:

- An additional short paragraph in the role summary 
- Additional key responsibilities based on the department context 
- Bespoke essential skills/experience required for the role